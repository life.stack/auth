FROM node:16-alpine as ts-compiler

WORKDIR /app
COPY .npmrc .
COPY tsconfig*.json ./
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build:prod

FROM node:16-alpine as ts-remover
WORKDIR /app
COPY .npmrc ./
COPY --from=ts-compiler /app/package*.json ./
COPY --from=ts-compiler /app/build ./
RUN npm install --only=production

#FROM gcr.io/distroless/nodejs:14
FROM node:16-alpine
ENV NODE_ENV=production
WORKDIR /app
COPY --from=ts-remover /app ./
USER 1000
CMD ["index.js"]