import express, { Request, Response } from 'express'
import { BadRequestError, currentUser, requireAuth } from '@life.stack/common'
import { Invite } from '../../models/invite'
import { Group } from '../../models/group'
import { GroupInvitationCreatedPublisher } from '../../events/publisher/groupInvitationCreatedPublisher'
import { natsWrapper } from '../../natsWrapper'

const router = express.Router()

const addDays = (date: Date, days: number) => {
  const copy = new Date(Number(date))
  copy.setDate(date.getDate() + days)
  return copy
}

router.post(
  '/api/groups/:id/invite',
  currentUser,
  requireAuth,
  async (req: Request, res: Response) => {
    const { id: groupId } = req.params
    const group = await Group.findById(groupId)
    if (!group || !group.owners.find((owner) => owner == req.currentUser?.id)) {
      throw new BadRequestError('Unable to invite to this group')
    }

    const invite = Invite.build({ group: group.id })
    await invite.save()

    const createdAt = new Date(Number(invite.createdAt))

    new GroupInvitationCreatedPublisher(natsWrapper.client).publish({
      id: invite._id,
      expiresAt: addDays(createdAt, 1).toISOString(),
    })

    res.status(201).send(invite)
  }
)

export { router as inviteGroupRouter }
