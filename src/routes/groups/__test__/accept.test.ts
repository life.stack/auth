import { app } from '../../../app'
import request from 'supertest'
import { UserDoc } from '../../../models/user'
import { Group } from '../../../models/group'
import { Invite } from '../../../models/invite'

const createUser = async (): Promise<UserDoc> => {
  const { body: signupUser } = await request(app)
    .post('/api/users/signup')
    .send({
      displayName: 'Testy',
      email: 'grouptest@test.com',
      password: 'passwordhelloworld',
    })

  return signupUser
}

it('has a route handler listening to /api/groups/accept for POST requests', async () => {
  const response = await request(app).post(`/api/groups/accept`).send()

  expect(response.status).not.toEqual(404)
})

it('can only be access if user is signed in', async () => {
  await request(app).post('/api/groups/accept').send().expect(401)
})

it('accepts a valid invite code and removes invite', async () => {
  const signupUser = await createUser()

  const { body: group } = await request(app)
    .post('/api/groups')
    .set('Cookie', global.signin(signupUser.id))
    .send({
      name: 'Our Family',
    })
    .expect(201)

  const { body: invite } = await request(app)
    .post(`/api/groups/${group.id}/invite`)
    .set('Cookie', global.signin(signupUser.id))
    .send({})
    .expect(201)

  const { body: joiningUser } = await request(app)
    .post('/api/users/signup')
    .send({
      displayName: 'Joiner',
      email: 'joining@test.com',
      password: 'passwordhelloworld',
    })

  const { body: acceptBody } = await request(app)
    .post(`/api/groups/accept`)
    .set('Cookie', global.signin(joiningUser.id))
    .send({ inviteKey: invite.inviteKey })
    .expect(201)

  const groupDoc = await Group.findById(group.id)
  expect(groupDoc?.users[1].toString()).toEqual(joiningUser.id)

  const inviteDoc = await Invite.findById(invite.id)
  expect(inviteDoc).toEqual(null)
})

it('will not allow user to join a group more than once', async () => {
  const signupUser = await createUser()

  const { body: group } = await request(app)
    .post('/api/groups')
    .set('Cookie', global.signin(signupUser.id))
    .send({
      name: 'Our Family',
    })
    .expect(201)

  const { body: invite } = await request(app)
    .post(`/api/groups/${group.id}/invite`)
    .set('Cookie', global.signin(signupUser.id))
    .send({})
    .expect(201)

  const { body: joiningUser } = await request(app)
    .post('/api/users/signup')
    .send({
      displayName: 'Joiner',
      email: 'joining@test.com',
      password: 'passwordhelloworld',
    })

  const { body: acceptBody } = await request(app)
    .post(`/api/groups/accept`)
    .set('Cookie', global.signin(joiningUser.id))
    .send({ inviteKey: invite.inviteKey })
    .expect(201)

  const { body: secondInvite } = await request(app)
    .post(`/api/groups/${group.id}/invite`)
    .set('Cookie', global.signin(signupUser.id))
    .send({})
    .expect(201)

  const { body: secondAccept } = await request(app)
    .post(`/api/groups/accept`)
    .set('Cookie', global.signin(joiningUser.id))
    .send({ inviteKey: secondInvite.inviteKey })
    .expect(400)
})
