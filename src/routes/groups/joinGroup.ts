import express, { Request, Response } from 'express'
import { BadRequestError, currentUser } from '@life.stack/common'
import { Group } from '../../models/group'
import { User } from '../../models/user'
import generateJWT from '../../services/generateJWT'
import { requireAuthButNotGroup } from '../../middleware/requireAuthButNotGroup'

const router = express.Router()

router.post(
  '/api/groups/:id/join',
  currentUser,
  requireAuthButNotGroup,
  async (req: Request, res: Response) => {
    const group = await Group.findById(req.params.id)
    let user = await User.findById(req.currentUser!.id)
    if (!group || !user) {
      throw new BadRequestError('Unable to join group')
    }

    group.users.push(req.currentUser!.id)
    await group.save()

    user.groups.push(group.id)
    user = await user.save()

    // Generate JWT
    const userJwt = generateJWT(user)

    // Store JWT on the session
    req.session = { jwt: userJwt }

    res.status(201).send(group)
  }
)

export { router as joinGroupRouter }
