import express from 'express'
import { UserProfile } from '../../models/userProfile'
import {
  currentUser,
  UserProfileSchema,
  validateBody,
} from '@life.stack/common'
import { requireAuthButNotGroup } from '../../middleware/requireAuthButNotGroup'

const createUserProfilesRouter = express.Router()

createUserProfilesRouter.post(
  '/api/users/profile',
  currentUser,
  requireAuthButNotGroup,
  validateBody(UserProfileSchema, async (req, res) => {
    let userProfile = await UserProfile.findById(req.currentUser!.id)

    if (!userProfile) {
      userProfile = new UserProfile({ _id: req.currentUser!.id })
    }

    console.log('3', req.body)

    userProfile.set(req.body)
    userProfile.save()

    res.status(200).send(userProfile)
  })
)

export { createUserProfilesRouter }
