import express, { Request, Response } from 'express'
import { currentUser } from '@life.stack/common'
import { UserProfile } from '../../models/userProfile'

const getUserProfileRouter = express.Router()

getUserProfileRouter.get(
  '/api/users/profile',
  currentUser,
  async (req: Request, res: Response) => {
    const userProfile = await UserProfile.findById(req.currentUser!.id)

    res.status(200).send(userProfile)
  }
)

export { getUserProfileRouter }
