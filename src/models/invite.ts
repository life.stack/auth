import mongoose from 'mongoose'
import { GroupDoc } from './group'
const en = require('nanoid-good/locale/en')
const customAlphabet = require('nanoid-good').customAlphabet(en)
const nanoid = customAlphabet(
  'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
  10
)

// An interface that describes the properties that are required to create a new group
interface InviteAttributes {
  group: string
}

// An interface that describes the properties that a group model has
interface InviteModel extends mongoose.Model<InviteDoc> {
  build(attributes: InviteAttributes): InviteDoc
}

// an interface that describes the properties a group document has
interface InviteDoc extends mongoose.Document {
  group: GroupDoc
  createdAt: Date
  inviteKey: string
}

const inviteSchema = new mongoose.Schema(
  {
    group: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Group',
    },
    createdAt: {
      type: Date,
      required: true,
    },
    inviteKey: {
      type: String,
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

inviteSchema.statics.build = (attributes: InviteAttributes) => {
  return new Invite({
    ...attributes,
    createdAt: new Date(),
    inviteKey: nanoid(),
  })
}

const Invite = mongoose.model<InviteDoc, InviteModel>('Invite', inviteSchema)

export { Invite }
