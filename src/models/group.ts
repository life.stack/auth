import mongoose from 'mongoose'
import { updateIfCurrentPlugin } from 'mongoose-update-if-current'

// An interface that describes the properties that are required to create a new group
interface GroupAttributes {
  name: string
  owner: string
}

// An interface that describes the properties that a group model has
interface GroupModel extends mongoose.Model<GroupDoc> {
  build(attributes: GroupAttributes): GroupDoc
}

// an interface that describes the properties a group document has
export interface GroupDoc extends mongoose.Document {
  name: string
  owners: Array<string>
  users: Array<string>
  version: number
}

const groupSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    owners: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
    users: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

groupSchema.set('versionKey', 'version')
groupSchema.plugin(updateIfCurrentPlugin)

groupSchema.statics.build = (attributes: GroupAttributes) => {
  return new Group({
    ...attributes,
    owners: [attributes.owner],
    users: [attributes.owner],
  })
}

const Group = mongoose.model<GroupDoc, GroupModel>('Group', groupSchema)

export { Group }
