import {
  GroupInvitationCreatedEvent,
  Publisher,
  Subjects,
} from '@life.stack/common'

export class GroupInvitationCreatedPublisher extends Publisher<GroupInvitationCreatedEvent> {
  subject: Subjects.GroupInvitationCreated = Subjects.GroupInvitationCreated
}
