import { queueGroupName } from './queueGroupName'
import { Message } from 'node-nats-streaming'
import {
  GroupInvitationExpiredEvent,
  Listener,
  Subjects,
} from '@life.stack/common'
import { Invite } from '../../models/invite'

export class GroupInvitationExpiredListener extends Listener<GroupInvitationExpiredEvent> {
  subject: Subjects.GroupInvitationExpired = Subjects.GroupInvitationExpired
  queueGroupName = queueGroupName

  async onMessage(data: GroupInvitationExpiredEvent['data'], msg: Message) {
    const invite = await Invite.findByIdAndDelete(data.invitationId)

    msg.ack()
  }
}
