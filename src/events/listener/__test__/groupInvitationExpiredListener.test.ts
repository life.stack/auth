import { natsWrapper } from '../../../natsWrapper'
import { Message } from 'node-nats-streaming'
import { GroupInvitationExpiredListener } from '../groupInvitationExpiredListener'
import { Invite } from '../../../models/invite'
import { Group } from '../../../models/group'
import { User } from '../../../models/user'
import { GroupInvitationExpiredEvent } from '@life.stack/common'

const setup = async () => {
  const listener = new GroupInvitationExpiredListener(natsWrapper.client)

  const user = User.build({
    email: 'listener@test.com',
    password: 'reallylongrealpassword.com123',
  })
  await user.save()

  const group = Group.build({
    name: 'Listener Group',
    owner: user.id,
  })
  await group.save()

  const invite = Invite.build({
    group: group.id,
  })
  await invite.save()

  const data: GroupInvitationExpiredEvent['data'] = {
    invitationId: invite.id,
  }

  // @ts-ignore
  const msg: Message = {
    ack: jest.fn(),
  }

  return { listener, invite, data, msg }
}

it('removes the invite from the database', async () => {
  const { listener, invite, msg, data } = await setup()

  await listener.onMessage(data, msg)

  const inviteInDatabase = await Invite.findById(invite.id)

  expect(inviteInDatabase).toEqual(null)
})

it('acks the message', async () => {
  const { listener, msg, data } = await setup()

  await listener.onMessage(data, msg)

  expect(msg.ack).toHaveBeenCalled()
})
